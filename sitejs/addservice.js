var categoryid = 0,
    listselcat = 0,
    update_id;
$('.addservice,.updateserviceldr').hide();
$(function() {
    loadcategoryfunc();
    loadservices();
});


//function to load category
function loadcategoryfunc() {
    $.ajax({
        url: listcategory_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#category_id,#add_category,#edit_category').append(`<option value="${data[i].id}">${data[i].name}</option>`);
                }
            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}
//function to load category ends here
//load service function starts here
function loadservices() {
    $('.imageclass').empty();
    if (listselcat == 0) {
        categoryid = 1;
    } else {
        categoryid = listselcat;
    }

    $.ajax({
        url: listservicesmanage_api + categoryid,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('#loaddetails').empty();
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#loaddetails').append(`<div class="col-md-3">
                        <div class="row slotsection">
                            <div class="col-md-10 nopadding">
                                <h5 class="mt-0 mb-5 fw500 nameclass${data[i].id}" value="${data[i].id}">${data[i].name}</h5>
                            </div>
                            <div class="col-md-2 floatright">
                                <a onclick="getid(${data[i].id})" class="pointer" data-toggle="modal" data-target="#editslotsmodal"><i class="ti-pencil"></i></a>
                            </div>
                        </div>
                    </div>`)
                }
            }else{
                $('.imageclass').empty().append(`<center><img src="images/logo.png" alt="home" class="light-logo" style="width": 20% !important><br><p class="classnodata">No Data Found</p></center>`);
            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}
//load service function ends here
//onchange function starts here
$("#category_id").change(function() {
    listselcat = $('#category_id').val();
    loadservices();
});

//add services function starts here
function addservice() {
    var addservicetag = [];
    // format 
    // [
    // {
    //     "sub_catgeory":1,
    //     "name":"Type 100"
    // },
    // {
    //     "sub_catgeory":1,
    //     "name":"Type 101"
    // }
    // ]

    if ($('.tagsadd').find('.bootstrap-tagsinput span.tag.label.label-info').length == 0) {
        $("#snackbarerror").text("Enter a service name");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    for (var j = 0; j < $('.tagsadd').find('.bootstrap-tagsinput span.tag.label.label-info').length; j++) {
        addservicetag.push({ "sub_category": $('#add_category').val(), name: $('.tagsadd').find(".bootstrap-tagsinput span.tag.label.label-info").eq(j).text() });
    }

    $('.addservice').show();

    console.log(addservicetag);
    var postData = JSON.stringify(
         addservicetag
    );
    $.ajax({
        url: addservicemultiple_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('.addservice').hide();
            $('.addservclose').click();
            $('span.tag.label.label-info').remove();
            $('#add_category').val(1);
            loadservices();
            $("#snackbarsuccs").text("Service has been created successfully");
            showsuccesstoast();
        },
        error: function(data) {
            $('.addservice').hide();

            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

//get id function starts here
function getid(id) {
    update_id = id;
    $('#edit_name').val($('.nameclass' + id).text());
}

//update service function starts here
function updateservice() {

    if ($('#edit_name').val() == '') {
        $("#snackbarerror").text("Enter a service name to update");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    $('.updateserviceldr').show();
    var postData = JSON.stringify({
        "sub_category": categoryid,
        "name": $('#edit_name').val()
    })
    $.ajax({
        url: updateservice_api + update_id + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('.updateserviceldr').hide();
            $('.updateclose').click();
            loadservices();
            $("#snackbarsuccs").text("Service has been updated successfully");
            showsuccesstoast();
        },
        error: function(data) {
            $('.updateserviceldr').hide();

            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}