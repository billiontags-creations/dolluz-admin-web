$(".setpassldr").hide();

function getQueryStrings() {
    var assoc = {};
    var decode = function(s) {
        return decodeURIComponent(s.replace(/\+/g, " "));
    };
    var queryString = location.search.substring(1);
    var keyValues = queryString.split('&');
    for (var i in keyValues) {
        var key = keyValues[i].split('=');
        if (key.length > 1) {
            assoc[decode(key[0])] = decode(key[1]);
        }
    }
    return assoc;
}

var qs = getQueryStrings();
sessionStorage.useridactivate = qs["id"];
sessionStorage.tokenactivate = qs["token"];



//reset password fn starts here
function setpwd() {

    if ($('#password').val().trim() == '') {
        $('#password').addClass("iserror");
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#password').val().length < 6) {
        $('#password').addClass("iserror");
        $("#snackbarerror").text("Min 6 Characters is Required for Password!");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#reenterpassword').val().trim() == '') {
        $('#reenterpassword').addClass("iserror");
        $("#snackbarerror").text("Re-enter password is required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#reenterpassword').val().length < 6) {
        $('#reenterpassword').addClass("iserror");
        $("#snackbarerror").text("Min 6 Characters is Required for Re-Enter Password!");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#reenterpassword').val() != $('#password').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".setpassldr").show();
    $(".setpassbtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;
    var postData = JSON.stringify({
        "uid": sessionStorage.useridactivate,
        "token": sessionStorage.tokenactivate,
        "client": uniqueclient,
        "password": $('#reenterpassword').val()

    });
    $.ajax({
        url: setpassword_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".setpassldr").hide();
            $(".setpassbtn").attr("disabled", false);
        },
        error: function(data) {
            $(".setpassldr").hide();
            $(".setpassbtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }).done(function(dataJson) {
        $("#snackbarsuccs").text("Password has been set successfully");
        showsuccesstoast();
        localStorage.userdetails = JSON.stringify(dataJson);
        localStorage.wutkn = dataJson.token;
        localStorage.first_name = dataJson.first_name;
        localStorage.emailid = dataJson.email;
        localStorage.phoneno = dataJson.username;
        setTimeout(function() {
            window.location.href = "dashboard.html";
        }, 3000);
    }); //done fn ends here
} //reset password fn ends here