var page_no = 1;

var filter = {
    limit: 24
};

var datetime = function datetime(x) {
    return new Date(x).toLocaleString('en-IN', { year: 'numeric', day: '2-digit', month: 'short', hour: 'numeric', minute: 'numeric', hour12: true });
};

const createQuery = () => {
    filter.offset = page_no > 1 ? filter.limit * ( page_no - 1 ) : '';
    let query = [];
    for(var keys in filter){
        query.push(keys + "=" + filter[keys]);
    }
    return '?' + query.join('&');
}

$(() => {
    listData( eventspecialist_api + createQuery() );
    dateFilter();
})

const listData = Url => { 
    $(".dataldr").show(); 
    $.ajax({
        url: Url,
        type: "GET",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        }
    }).done(data => {
        $(".dataldr").hide();     
        document.getElementById("listTable").innerHTML = tableComponent(data.results);
        pagination(data.next, data.previous, data.count);
    }).fail(data => {
        console.log(data);
        $(".dataldr").hide(); 
        $(".paginator").empty();            
    })
}


const tableComponent = data => ( data.length ?
`<table class="table table-hover manage-u-table headclassuser">
    <thead>
        <tr>
            <th width="70" class="text-center">S.No</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Time to call</th>
            <th>Created On</th>
        </tr>
    </thead>
    <tbody>
        ${tableBody(data)}
    </tbody>
</table>` : 
`<center>
    <img src="images/logo.png" alt="home" class="light-logo" style="width:20% !important">
    <br><p class="classnodata">No Data Found</p>
</center>`);


const tableBody = data => {
    let row = ``;
    data.map((tr, index) => {
        row += 
        `<tr>
            <td class="text-center">${( (page_no - 1) * filter.limit ) + index + 1}</td>
            <td>${tr.name}</td>
            <td>${tr.mobile_number}</td>
            <td>${tr.email}</td>
            <td>${tr.time_to_call}</td>
            <td>${datetime(tr.created_on)}</td>
        </tr>`
    })
    return row;
}

//pagination starts here
const pagination = (next, prev, count) => {
    $(".paginator").empty();
    var pages = Math.ceil(count / filter.limit);
    if (pages > 1) {
        prev ? $(".paginator").append(`<li><a onclick="prevpage('${prev}')"><i class="fa fa-angle-left"></i></a></li>`) : '';
        for (var i = (page_no < 3 ? 1 : page_no - 2); i <= pages && i <= (page_no < 3 ? page_no + (5 - page_no) : page_no + 2); i++) {
            $(".paginator").append(`<li class="pgs pg${i}"><a onclick="navigate(${i})">${i}</a></li>`);
        }
        next ? $(".paginator").append(`<li><a onclick="nextpage('${next}')"><i class="fa fa-angle-right"></i></a></li>`) : '';
    }
    $(".pgs").removeClass("active");
    $(".pg" + page_no).addClass("active");
}

const navigate = page => {
    page_no = Number(page);
    listData( eventspecialist_api + createQuery() );
}

const nextpage = next => {
    page_no++;
    listData(next);
}

const prevpage = prev => {
    page_no--;
    listData(prev);
}

//filters
const reloadData = () => {
    page_no = 1;
    listData( eventspecialist_api + createQuery() );    
}

const nameFilter = () => {
    filter.q = $(".searchbyname").val();
    reloadData();
}

const dateFilter = () => {
    $('.input-daterange-datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        filter.created_min = picker.startDate.format('YYYY-MM-DD');
        filter.created_max = picker.endDate.format('YYYY-MM-DD');
        reloadData();
    });
}

const showallfunc = () => {
    filter.q = '';
    filter.created_min = '';
    filter.created_max = '';
    $(".searchbyname, .input-daterange-datepicker").val('');
    reloadData();
}

function exportascsv(){
    window.location.href = eventspecialist_api + "export/" + createQuery();
}