$(function() {

    $(".lgnldr,.changepasswordldr,.recoverpassldr,.resetpwdldr").hide();

    //enter click fn signup/login starts here
    $(".ipclklgn").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclklgn").click();
        }
    });

    //edit password
    $(".editpass").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".changepasswordbtn").click();
        }
    });

    //forgot password
    $(".frtpasstxt").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".recoverpassbtn").click();
        }
    });

});

$("#loginphoneno").keyup(function() {
    if ($("#loginphoneno").val() == "") {
        $('#loginphoneno').addClass("iserror");
    } else {
        $('#loginphoneno').removeClass("iserror");
    }
});

$("#loginpasswrd").keyup(function() {
    if ($("#loginpasswrd").val() == "")
        $('#loginpasswrd').addClass("iserror");
    else
        $('#loginpasswrd').removeClass("iserror");

});

$("input").keyup(function() {
    $(this).removeClass("iserror");
});

//user login fn starts here
function login() {

    if ($('#loginphoneno').val().trim() == '') {
        $('#loginphoneno').addClass("iserror");
        $("#snackbarerror").text("E-Mail Address / Phone No is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
    var str = $('#loginphoneno').val();
    if (numberRegex.test(str)) {
        var phone = $('#loginphoneno').val();
        var phoneNum = phone.replace(/[^\d]/g, '');
        if (phoneNum.length < 10 || phoneNum.length > 11) {
            $('#loginphoneno').addClass("iserror");
            $("#snackbarerror").text("Valid Phone No is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
    } else {
        var x = $('#loginphoneno').val();
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
            $('#loginphoneno').addClass("iserror");
            $("#snackbarerror").text("Valid E-Mail Address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
    }

    if ($('#loginpasswrd').val().trim() == '') {
        $('#loginpasswrd').addClass("iserror");
        $("#snackbarerror").text("Password is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#loginpasswrd').val().length < 6) {
        $('#loginpasswrd').addClass("iserror");
        $("#snackbarerror").text("Atleast 6 character length password is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    $(".lgnldr").show();
    $(".lgnBtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;
    var postData = JSON.stringify({

        "username": $('#loginphoneno').val(),
        "password": $('#loginpasswrd').val(),
        "client": uniqueclient,
        "role": [1, 2]

    });

    $.ajax({
        url: login_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            $(".lgnldr").hide();
            $(".lgnBtn").attr("disabled", false);
            localStorage.userdetails = JSON.stringify(data);
            localStorage.userid = data.id;
            localStorage.wufname = data.first_name;
            localStorage.wutkn = data.token;
            localStorage.wuemail = data.email;
            localStorage.username = data.username;
            localStorage.userrole = data.userprofile.role;

            $("#snackbarsuccs").text("Login Success!");
            showsuccesstoast();
            $('#loginphoneno').val('');
            $('#loginpasswrd').val('');

            window.location.href = 'dashboard.html';

        },
        error: function(data) {

            $(".lgnldr").hide();
            $(".lgnBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();

        }
    })

} //login fn ends here

//change password fn starts here
function changepassword() {

    if ($('#oldpassword').val().trim() == '') {
        $("#oldpassword").addClass("iserror");
        $("#snackbarerror").text("Old Password is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#newpassword').val().trim() == '') {
        $("#newpassword").addClass("iserror");
        $("#snackbarerror").text("New Password is Required");
        showerrtoast();
        event.stopPropagation();
        return;
    } else if ($('#newpassword').val().length < 6) {
        $("#oldpassword").addClass("iserror");
        $("#snackbarerror").text("Old Password must have minimum 6 character");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#oldpassword').val() == $('#newpassword').val()) {
        $("#snackbarerror").text("Old Password & New Password should not be same!");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#renewpassword').val().trim() == '') {
        $("#renewpassword").addClass("iserror");
        $("#snackbarerror").text("Re - New Password is Required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#newpassword').val() != $('#renewpassword').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    $(".changepasswordldr").show();
    $(".changepasswordbtn").attr("disabled", true);

    var postdata = JSON.stringify({
        "old_password": $("#oldpassword").val(),
        "new_password": $("#newpassword").val()
    });
    $.ajax({
        url: changepassword_api,
        type: 'post',
        data: postdata,
        headers: {
            "content-type": 'application/json'
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".changepasswordldr").hide();
            $(".changepasswordbtn").attr("disabled", false);
            $("#snackbarsuccs").text("Password Changed Sucessfully!");
            showsuccesstoast();
            $("#oldpassword,#newpassword,#renewpassword").val("");
            window.location.href = "dashboard.html"
        },
        error: function(data) {
            $(".changepasswordldr").hide();
            $(".changepasswordbtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
} //change password fn ends here

//FORGOT PASSWORD FUNCTION STARTS HERE
function forgotpassword() {

    if ($('#phonenumber').val().trim() == '') {
        $('#phonenumber').addClass("iserror");
        $("#snackbarerror").text("Phone number is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    $(".recoverpassldr").show();
    $(".recoverpassbtn").attr("disabled", true);

    // var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({
        "mobile_number": $('#phonenumber').val(),
        "role": [1, 2]
    });
    //ajax starts here
    $.ajax({
        url: forgotpwd_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            sessionStorage.forgotphoneno = $('#phonenumber').val();
            $(".recoverpassldr").hide();
            $(".recoverpassbtn").attr("disabled", false);
        },
        error: function(data) {
            $(".recoverpassldr").hide();
            $(".recoverpassbtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }).done(function(dataJson) {
        $("#snackbarsuccs").text("OTP has been sent to your mobile!");
        showsuccesstoast();
        setTimeout(function() { window.location.href = "reset-password.html"; }, 3000);
    }); //done fn ends here
    //ajax ends here

} //frgtpassword fn ends here


//reset password fn starts here
function restpwd() {
    
    if ($('#rsotp').val().trim() == '') {
        $('#rsotp').addClass("iserror");
        $("#snackbarerror").text("OTP is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#password').val().trim() == '') {
        $('#password').addClass("iserror");
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#password').val().length < 6) {
        $('#password').addClass("iserror");
        $("#snackbarerror").text("Min 6 Characters is Required for Password!");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#retype_password').val().trim() == '') {
        $('#retype_password').addClass("iserror");
        $("#snackbarerror").text("Re-enter password is required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#retype_password').val().length < 6) {
        $('#retype_password').addClass("iserror");
        $("#snackbarerror").text("Min 6 Characters is Required for Re-Enter Password!");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#retype_password').val() != $('#password').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".resetpwdldr").show();
    $(".resetpwdBtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;
    var postData = JSON.stringify({
        "role": [1, 2],
        "client": uniqueclient,
        "password": $('#password').val(),
        "mobile_number": sessionStorage.forgotphoneno,
        "otp": $('#rsotp').val()

    });
    $.ajax({
        url: resetpassword_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".resetpwdldr").hide();
            $(".resetpwdBtn").attr("disabled", false);
        },
        error: function(data) {
            $(".resetpwdldr").hide();
            $(".resetpwdBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }).done(function(dataJson) {
        $("#snackbarsuccs").text("Password has been set successfully");
        showsuccesstoast();
        localStorage.userdetails = JSON.stringify(dataJson);
        localStorage.wutkn = dataJson.token;
        localStorage.first_name = dataJson.first_name;
        localStorage.emailid = dataJson.email;
        localStorage.phoneno = dataJson.username;
        setTimeout(function() {
            window.location.href = "dashboard.html";
        }, 3000);
    }); //done fn ends here
} //reset password fn ends here