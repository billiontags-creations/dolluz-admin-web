var datetime = function datetime(x) {
    return new Date(x).toLocaleString('en-IN', { year: 'numeric', day: '2-digit', month: 'short', hour: 'numeric', minute: 'numeric', hour12: true });
};

//validation
function emptyvalid(cl) {
    return cl.val().length > 0;
}

function emailvalid(cl) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(cl.val());
}

function phonevalid(cl) {
    var re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return re.test(cl.val());
    // console.log(cl.val().test(re));
}

function ldrfail(btn, ldr) {
    ldr.hide();
    btn.attr("disabled", false);
}

function ldrpass(btn, ldr) {
    ldr.show();
    btn.attr("disabled", true);
}

var search = "";

$(() => {
    listData(organizer_api);
})

const listData = Url => {
    $(".dataldr").show(); 
    $.ajax({
        url: Url + "?q=" + search,
        type: "GET",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        }
    }).done(data => {
        $(".dataldr").hide();     
        document.getElementById("listTable").innerHTML = tableComponent(data);
        switchery();
    }).fail(data => {
        console.log(data);      
        $(".dataldr").hide();             
    })
}


const tableComponent = data => ( data.length ?
`<table class="table table-hover manage-u-table headclassuser">
    <thead>
        <tr>
            <th width="70" class="text-center">S.No</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Created On</th>
            <th>Edit</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        ${tableBody(data)}
    </tbody>
</table>` : 
`<center>
    <img src="images/logo.png" alt="home" class="light-logo" style="width:20% !important">
    <br><p class="classnodata">No Data Found</p>
</center>`);


const tableBody = data => {
    let row = ``;
    data.map((tr, index) => {
        row += 
        `<tr class="active${tr.is_active}">
            <td class="text-center">${index + 1}</td>
            <td>${tr.name}</td>
            <td>${tr.mobile_number}</td>
            <td>${tr.email}</td>
            <td>${datetime(tr.created_on)}</td>
            <td><a onclick="openmodal(${tr.id})"><i class="fa fa-pencil"></i></a></td>
            <td>
                <input type="checkbox" class="js-switch commonclass" data-color="#f96262" data-size="small" ${tr.is_active ? "checked" : ""} onchange="activate(${tr.id}, ${tr.is_active})" />
            </td>
        </tr>`
    })
    return row;
}

//filters
const reloadData = () => {
    listData( organizer_api );    
}

const nameFilter = () => {
    search = $(".searchbyname").val();
    reloadData();
}

const showallfunc = () => {
    search = "";
    $(".searchbyname").val('');
    reloadData();
}

// Switchery
const switchery = () => {
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });
}

const openmodal = id => {
    $(".organizermodal_title").text(id ? "Edit Organizer" : "Add Organizer");
    $("#organizermodal input").val('');
    if(!id){
        $(".organizerbtn").attr("onclick", `addorganizer()`);
        $("#organizermodal").modal("show");
    } else{
        retrieveorganizer(id);
    }
}

//Retrive organizer
const retrieveorganizer = id => {
    $.ajax({
        url: organizer_api + id + "/",
        type: "get",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        }
    }).done(data => {
        $("#organizer_name").val(data.name);
        $("#organizer_phone").val(data.mobile_number);
        $("#organizer_email").val(data.email);
        $(".organizerbtn").attr("onclick", `editorganizer(${data.id})`);
        $("#organizermodal").modal("show");
    }).fail(data => {
        console.log(data);
    })
}

//Add organizer
const addorganizer = () => {
    if(!emptyvalid($("#organizer_name"))){
        $("#snackbarerror").text("Name is required");
        showerrtoast();
        return;
    }
    if(!emptyvalid($("#organizer_phone"))){
        $("#snackbarerror").text("Phone Number is required");
        showerrtoast();
        return;
    }
    if(!phonevalid($("#organizer_phone"))){
        $("#snackbarerror").text("Phone Number is invaild");
        showerrtoast();
        return;
    }
    if(!emptyvalid($("#organizer_email"))){
        $("#snackbarerror").text("Email is required");
        showerrtoast();
        return;
    }
    if(!emailvalid($("#organizer_email"))){
        $("#snackbarerror").text("Email is invalid");
        showerrtoast();
        return;
    }
    ldrpass($(".organizerbtn"), $(".organizerldr"));
    $.ajax({
        url: organizer_api,
        type: "post",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        data: JSON.stringify({
            name: $("#organizer_name").val(),
            mobile_number: $("#organizer_phone").val(),
            email: $("#organizer_email").val()
        })
    }).done(data => {
        reloadData();
        ldrfail($(".organizerbtn"), $(".organizerldr"));
        $("#organizermodal input").val('');
        $(".addsubmodalclose").click();
        $("#snackbarsuccs").text("Add successfull");
        showsuccesstoast();
    }).fail(data => {
        console.log(data);
        ldrfail($(".organizerbtn"), $(".organizerldr"));  
        commmonErrmsg(data);      
    })
}

const editorganizer = id => {
    if(!emptyvalid($("#organizer_name"))){
        $("#snackbarerror").text("Name is required");
        showerrtoast();
        return;
    }
    if(!emptyvalid($("#organizer_phone"))){
        $("#snackbarerror").text("Phone Number is required");
        showerrtoast();
        return;
    }
    if(!phonevalid($("#organizer_phone"))){
        $("#snackbarerror").text("Phone Number is invaild");
        showerrtoast();
        return;
    }
    if(!emptyvalid($("#organizer_email"))){
        $("#snackbarerror").text("Email is required");
        showerrtoast();
        return;
    }
    if(!emailvalid($("#organizer_email"))){
        $("#snackbarerror").text("Email is invalid");
        showerrtoast();
        return;
    }
    ldrpass($(".organizerbtn"), $(".organizerldr"));
    $.ajax({
        url: organizer_api + id + "/",
        type: "put",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        data: JSON.stringify({
            name: $("#organizer_name").val(),
            mobile_number: $("#organizer_phone").val(),
            email: $("#organizer_email").val()
        })
    }).done(data => {
        reloadData();
        ldrfail($(".organizerbtn"), $(".organizerldr"));
        $("#organizermodal input").val('');
        $(".addsubmodalclose").click();
        $("#snackbarsuccs").text("Edit successfull");
        showsuccesstoast();
    }).fail(data => {
        console.log(data);
        ldrfail($(".organizerbtn"), $(".organizerldr"));
        commmonErrmsg(data);        
    })
}

//change status 
const activate = (id, active) => {
    $.ajax({
        url: organizer_api + id + "/",
        type: "patch",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        data: JSON.stringify({
            is_active: !active
        })
    }).done(data => {
        reloadData();
    }).fail(data => {
        console.log(data);  
        commmonErrmsg(data);    
    })
}