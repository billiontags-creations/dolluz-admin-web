var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    toggleid, changeid, color_array = ["bg-info", "bg-success", "bg-warning", "bg-danger", "bg-inverse"];
$('.enableldr,.showalldr,.updateldr').hide();
var send_start, send_end, nxt;
$(function() { //initial function starts here
    pagesallowed();    
    loadmanagevendor(1);
    $("#adminpassword").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".enablebtn").click();
        }
    });
    $(".editpass").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".updatebtn").click();
        }
    });
    $('.input-daterange-datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        $('#urlloader').empty();
        $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
        send_start = picker.startDate.format('YYYY-MM-DD');
        send_end = picker.endDate.format('YYYY-MM-DD');
        loadmanagevendor(2);
    });
}); //initial function ends here

function loadmanagevendor(type) { //load manager function starts here
    if (type == 1) {
        var postData = JSON.stringify({
            "search": "",
            "start_date": "",
            "end_date": ""
        });
        var url = listvendor_api;
    } else if (type == 2) {
        
        var post = {};
        $('.searchbyname').val() ? post.search = $('.searchbyname').val() : '';
        send_start ? post.start_date = send_start : '' ; 
        send_end ? post.end_date = send_end : ''; 
        var postData = JSON.stringify(post);
        var url = listvendor_api;

    } else {
        var url = type;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('input[name="daterangepicker_start"]').val("");
            $('input[name="daterangepicker_end"]').val("");

            $('.showalldr,updateldr').hide();


            $('.userbodayclass').empty();
            $('.imgappendclass').empty();

            if (data.results.length != 0) {
                $('.headclassuser').show();

                for (var i = 0; i < data.results.length; i++) {
                    //joined date
                    var datalogin1 = data.results[i].date_joined.split('T')
                    var content1 = datalogin1[0].split('-');
                    var date_join = content1[2] + ' ' + month[content1[1] - 1] + ' ' + content1[0];

                    var enddat = tConvert(datalogin1[1].slice(0, 8));

                    //last login date
                    if (data.results[i].last_login == null) {
                        var date_log = '-';
                        var startdat = '';
                    } else {
                        var datalogin = data.results[i].last_login.split('T')
                        var content = datalogin[0].split('-');
                        var date_log = content[2] + ' ' + month[content[1] - 1] + ' ' + content[0];

                        var startdat = tConvert(datalogin[1].slice(0, 8));
                    }

                    $('.userbodayclass').append(` <tr class="colorappend${data.results[i].id}">
                                    <td class="text-center">${i+1}</td>
                                    <td>${data.results[i].first_name}</td>
                                    
                                    <td>${data.results[i].username}</td>
                                    <td>${data.results[i].email ? data.results[i].email : 'Not Available'}</td>
                                    <td>
                                        <a onclick="getchangeid(${data.results[i].id});" class="viewanchor cptr" data-toggle="modal" data-target="#editpwdmodal">Edit Password</a>
                                    </td>
                                    <td><a onclick="listing(${data.results[i].id})" class="viewanchor cptr" data-toggle="modal" data-target="#alllistingsmodal">Click Here</a></td>
                                    <td>${date_log}
                                        <br/><span class="text-muted">${startdat}</span></td>
                                    <td>${date_join}
                                        <br/><span class="text-muted">${enddat}</span></td>
                                    <td>
                                        <input type="checkbox" class="js-switch commonclass switchclass${data.results[i].id}" value="${data.results[i].id}" onchange="myFunction(${data.results[i].id})" data-color="#f96262" data-size="small" />
                                    </td>
                                </tr>`);
                    (data.results[i].is_active) ? $('.switchclass' + data.results[i].id).attr("checked", "checked"): $('.colorappend' + data.results[i].id).addClass('colorclassactive');
                }
            } else {


                $('.headclassuser').hide();
                $('.imgappendclass').append(`<center><img src="images/logo.png" alt="home" class="light-logo" style="width": 20% !important><br><p class="classnodata">No Data Found</p></center>`);
            }

            if (data.next_url != null) {
                var next1 = (data.next_url).split('=');
                var val = data.count / data.page_size;
                if (val["colorField"] % 1 === 0) {
                    //if number is integer 
                    var val = val;
                } else {
                    var val = parseInt(val) + 1;
                }
                var obj = $('#pagination').twbsPagination({
                    totalPages: val,
                    visiblePages: 3,
                    onPageClick: function(event, page) {
                        console.info(page);
                        loadmanagevendor(next1[0] + "=" + (page));
                    }
                });
                console.info(obj.data());
            }

            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
} //load manager function ends here

function repeatArray(arr, count) {
    var ln = arr.length;
    for (i = 0; i < count; i++) {
        color_array.push(arr[i % ln]);
    }
}

//TIME CONVERTION FUNCTION
function tConvert(time) {
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
        time = time.slice(1);
        time[3] = +time[0] < 12 ? 'AM' : 'PM';
        time[0] = +time[0] % 12 || 12;
    }
    return time.join('');
}

function myFunction(id) {
    // $(this).is(':checked');
    $('#conformationmodal').modal();
    console.log(id);
    toggleid = id;
}

function enabledisable() { //function starts here
    if ($('#adminpassword').val() == '') {
        $('#adminpassword').addClass("iserror");
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $('.enableldr').show();
    $('.enablebtn').attr("disabled", true);

    var postData = JSON.stringify({
        "password": $('#adminpassword').val()
    });

    $.ajax({
        url: useractiverdiactivate_api + toggleid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('#urlloader').empty();
            $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
            $('.enableldr').hide();
            $('.enablebtn').attr("disabled", false);
            loadmanagevendor(1);
            $("#snackbarsuccs").text("User status changed successfully");
            showsuccesstoast();
            $('.adminpassclose').click();
            $('#adminpassword').val('')
        },
        error: function(data) {
            $('.enableldr').hide();
            $('.enablebtn').attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //function ends here

//ONCHANGE FUNCTION FOR SEARCH STARTS HRE
$('.searchresulttextbox').change(function() {
    loadmanagevendor(2);
});

$('.searchbyname').keyup(function() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    loadmanagevendor(2);
})

//showall filter func
function showallfunc() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    $('.searchbyname, .input-daterange-datepicker').val('');
    $('.showalldr').show();
    loadmanagevendor(1);
}

//get id function starts here
function getchangeid(getid) {
    changeid = getid
}


function loaddata() {
    $('.switchclass' + toggleid).click();
}

//on password change function starts here
function changepass() {
    if ($('#newpass').val() == '') {
        $('#newpass').addClass("iserror");
        $("#snackbarerror").text("New Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#renewpass').val() == '') {
        $('#renewpass').addClass("iserror");
        $("#snackbarerror").text("Re-enter New Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $('.updateldr').show();
    $('.updatebtn').attr("disabled", true);

    var postData = JSON.stringify({
        "password": $('#renewpass').val()
    });

    $.ajax({
        url: resetpassworduser_api + changeid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('.updateldr').hide();
            $('.updatebtn').attr("disabled", false);
            loadmanagevendor(1);
            $("#snackbarsuccs").text("Password changed successfully");
            showsuccesstoast();
            $('.editpassclose').click();
            $('#newpass,#newpass').val('')
        },
        error: function(data) {
            $('.updateldr').hide();
            $('.updatebtn').attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });

}

//listing function starts here
function listing(listid) {
    $.ajax({
        type: 'GET',
        url: listservices_api + listid + '/',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
    }).done(function(data) {
        $('.listservice').empty();

        repeatArray(color_array, data.length);

        for (var i = 0; i < data.length; i++) {
            $('.listservice').append(` <li style="cursor: pointer" onclick="viewservices(${i +1}, ${data[i].count}, ${listid})">
                                <div class="categoryicon categoryicon${i}"><i class="ti-user text-white"></i></div> ${data[i].name} <span class="text-muted"> ${data[i].count} </span></li>
                            <li>`);
            $('.categoryicon' + i).addClass(color_array[i]);

        }
    }).fail(function(data) {
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}

function viewservices(id, count, user){
    if(count){
        window.location.href = "manage-services.html?user=" + user + "&sub_category=" + id;
    } else {
        $("#snackbarerror").text("No services found");
        showerrtoast();        
    }
}

function exportascsv(){
    window.location.href = vendorcsv_api + `?search=${$('.searchbyname').val() ? $('.searchbyname').val() : ''}&start_date=${send_start ? send_start : '' }&end_date=${send_end ? send_end : '' }`;
}