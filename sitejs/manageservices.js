$(function() {

    $(".nodatarow").hide();
    listdata(1);

});

function listdata(type) {
    sessionStorage.type = type;

    if (type == 1) {
        var url = listing_services_api  + window.location.search;
    } else {
        var url = listing_services_api + window.location.search +"&page=" + sessionStorage.gotopage_no;
    }

    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            $(".dyn_listing").empty();
            $(".nodatarow").hide();
            sessionStorage.data = JSON.stringify(data);
            loadlisting_details();
        },
        error: function(data) {
            console.log("error occured in listing page");
            $(".dyn_listing").empty();
            $(".nodatarow").show();
            // $(".showallldr").hide();
            // $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
        }
    });
}

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
var datacount = 0;

function loadlisting_details() {
    var data = JSON.parse(sessionStorage.data);
    if (data.results.length == 0) {
        $(".dynpagination").empty().hide();
        // $(".dyn_listing").append(`<br><br><div class="row"><center><img src="nodata.jpg" style="width:40%"><br><br><p class="nolistdata">No Data Found</p></center></div>`)
        $(".nodatarow").show();
    } else {
        if (sessionStorage.type == 1) {
            pagination(data.count, data.prev_url, data.next_url);
            datacount = data.page_size;
        }
        for (var i = 0; i < data['results'].length; i++) {
            if (data['results'][i].address) {
                var addr = data['results'][i].address.street + ',' + data['results'][i].address.area + ',' + data['results'][i].address.city + ',' + data['results'][i].address.state + ',' + data['results'][i].address.country + ',' + data['results'][i].address.zipcode;
                var lat = data['results'][i].address.latitude;
                var long = data['results'][i].address.longitude;
            } else {
                var addr = "Not Mentioned";
                var lat = 0;
                var long = 0;
            }
            var types = [];
            if (data['results'][i].types.length != 0) {
                for (var j = 0; j < data['results'][i].types.length; j++) {
                    types.push(data['results'][i].types[j].type.name);
                }
            } else {
                types.push("Not Mentioned");
            }
            $(".dyn_listing").append(`<div class="col-sm-6">
                        <div class="white-box servicediv servicediv${data['results'][i].id}  ${data['results'][i].is_active ? '' : 'serviceinactive'}">
                            <div class="row">
                                <div class="col-sm-5">
                                    <p class="addserviceheading">${data['results'][i].sub_category.name}</p>
                                </div>
                                <div class="col-sm-7">
                                    <a class="savebtn mr-5 cptr" onclick="viewthis(${data['results'][i].id})">View More</a>
                                    <label style="margin-top: 5px;">
                                    <input type="checkbox" class="floatright js-switch" data-color="#f96262" data-size="small" onclick="deactivate(${data['results'][i].id})" ${data['results'][i].is_active ? 'checked' : ''}/>
                                    <span class="servicetxt${data['results'][i].id}">${data['results'][i].is_active ? 'Active' : 'Inactive'}</span>
                                    </label>
                                </div>
                            </div>
                            <hr class="mt-0 mb-10">
                            <div class="row">
                                <div class="col-md-3 pr-0">
                                    <img src="${data['results'][i].cover_picture ? data['results'][i].cover_picture : "images/logo.png"}" class="img-responsive listimghandw">
                                </div>
                                <div class="col-md-9">
                                    <p class="sportsclubname">${data['results'][i].name}</p>
                                    <p class="sportslocation">${addr}</p>
                                    <p class="detailstitle"><b>Types:</b>&nbsp;${types}</p>
                                </div>
                            </div>
                        </div>
                    </div>`);
        } //for loop ends here
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    } //else cond ends here
    // $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
    // $(".showallldr").hide();
}

//pagination fn starts here
function pagination(count, prev, next) {
    $(".dynpagination").empty().show().append(`<li style="${prev ? '' : 'display:none;'}"><a onclick="previouspage()" class="cptr"> <i class="ti-arrow-left"></i> </a></li>`);
    if ((count / datacount) % 1 == 0) { //should change here only for how many box should come // count
        var totalpagescount = (count / datacount);
    } else {
        var totalpagescount = parseInt((count / datacount), 10) + 1;
    }
    sessionStorage.totalpagescount = totalpagescount;
    for (var i = 0; i < totalpagescount; i++) {
        $(".dynpagination").append(`<li class="pageli pageli${i+1} ${(i == 0 ? "active" : "")}" pageno="${i+1}"> <a class="cptr" onclick="gotothispage(${i+1})">${i+1}</a> </li>`);
        if (i == (totalpagescount - 1)) {
            $(".dynpagination").append(`<li  style="style="${next ? '' : 'display:none;'}"><a onclick="nextpage()" class="cptr"> <i class="ti-arrow-right"></i> </a></li>`);
        }
    }
    $(".pageli").hide();
    sessionStorage.showcount = 5;
    for (var i = 1; i <= 5; i++) {
        $(".pageli" + i).show();
    }
}

function gotothispage(pageno) {
    $(".pageli").removeClass("active");
    $(".pageli" + pageno).addClass("active");
    sessionStorage.gotopage_no = pageno;
    listdata(2);
}

function nextpage() {
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    currentpageno++;
    if (parseInt(sessionStorage.totalpagescount) >= currentpageno) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);

        if ((currentpageno % 5) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
            $(".pageli").hide();
            for (var i = currentpageno; i <= currentpageno + 4; i++) {
                $(".pageli" + i).show();
            }
        }
    } //if cond ends here
}

function previouspage() {
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    currentpageno--;
    if (currentpageno != 0) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        if ((currentpageno % 5) == 0) {
            $(".pageli").hide();
            for (var i = currentpageno; i >= currentpageno - 4; i--) {
                $(".pageli" + i).show();
            }
        }
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);
    }
}

function viewthis(serviceid){
	sessionStorage.serviceid = serviceid;
	var url = 'http://dolluzweb-js.billioncart.com/hall-detail.html?id='+serviceid+'';
	window.open(url,'_blank');
}

function deactivate(id){
    var ajaxoptions = {
        url: deactivateservice_api + id + "/",
        type: "put",
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        }
    };
    $.ajax(ajaxoptions)
        .done(function(data){
            if(data.is_active){
                $(".servicetxt" + id).text("Active");
                $(".servicediv" + id).removeClass("serviceinactive");
                $("#snackbarsuccs").text("Activated");
                showsuccesstoast();
            } else {
                $(".servicetxt" + id).text("Inactive");                
                $(".servicediv" + id).addClass("serviceinactive");                
                $("#snackbarerror").text("Deactivated");
                showerrtoast();
            }   
        })
        .fail(function(data){
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        })
}


