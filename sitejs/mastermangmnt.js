var color_array = ["bg-info", "bg-success", "bg-warning", "bg-danger", "bg-inverse"];
$(function() {
    pagesallowed();    
    loadmanagement();
});

//load function starts here
function loadmanagement() {
    $.ajax({
        url: addfreelancers_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('.tablebody').empty();
            if (data.length != 0) {
                repeatArray(color_array, data.length);
                for (var i = 0; i < data.length; i++) {
                    if (data[i].transaction_percent == null) {
                        var val = 0;
                    } else {
                        var val = data[i].transaction_percent;
                    }
                    $('.tablebody').append(`<tr>
                                    <td class="text-center lh40">${i+1}</td>
                                    <td>
                                        <div class="categoryicon categoryicon${data[i].id}"><i class="ti-user text-white"></i></div> ${data[i].name}
                                    </td>
                                    <td class="lh40">${val}%</td>
                                    <td>
                                        <div class="form-material">
                                            <input type="text" value="${val}" name="example-email" class="form-control transcatprec${data[i].id}" placeholder="Eg.____">
                                        </div>
                                    </td>
                                    <td><a onclick="updatemanagement(${data[i].id})" class="roundbutton" type="submit"><i class="fa fa-paper-plane"></i></a></td>
                                </tr>`);

                    $('.categoryicon' + data[i].id).addClass(color_array[i]);

                }
            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}


function repeatArray(arr, count) {
    var ln = arr.length;
    for (i = 0; i < count; i++) {
        color_array.push(arr[i % ln]);
    }
}



function updatemanagement(id) {

    if ($('.transcatprec' + id).val() == "") {
        $("#snackbarerror").text("Enter Transaction Percentage");
        showerrtoast();
        event.stopPropagation();
        return;
    }


    var postData = JSON.stringify({
        "transaction_percent": $('.transcatprec' + id).val()
    })
    $.ajax({
        url: updatefreelancers_api + id + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            loadmanagement();
            $("#snackbarsuccs").text("Transaction Percentage has been updated successfully");
            showsuccesstoast();
        },
        failuer: function(data) {


            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}