var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

//success toast fn starts here
function showsuccesstoast() {
    var x = document.getElementById("snackbarsuccs");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 4000);
}

//failure toast fn starts here
function showerrtoast() {
    var x = document.getElementById("snackbarerror");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 4000);
}

//error toast for ip errors
function showiperrtoast() {
    var x = document.getElementById("snackbarerror");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 3500);
}

$(function() {
    $(document).ajaxError(function(event, jqxhr, settings, thrownError){
        jqxhr.status == 401 ? window.location.href = "index.html" : '';
    });
    $("body").append('<div id="snackbarsuccs"></div><div id="snackbarerror"></div>');
    $(".manufacturingprice,.manufacturingpricedit").keypress(function(e) {
        if ($(this).val().length > 5 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $("#side-menu").append(`
        <li> <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-arrow-down-drop-circle fa-fw"></i>More</a>
            <ul class="nav nav-second-level">
                <li><a href="contactus.html" class="waves-effect"><span class="hide-menu">Contact Us</span></a></li>
                <li><a href="event-specialist.html" class="waves-effect"><span class="hide-menu">Event Specialist</span></a></li>
                <li><a href="event-organizer.html" class="waves-effect"><span class="hide-menu">Event Organizer</span></a></li>
                <li><a href="organizer-management.html" class="waves-effect"><span class="hide-menu">Organizer Management</span></a></li>
            </ul>
        </li>
    `);
});

//logout fn starts here
function logout() {
    $.ajax({
        url: logout_api,
        type: 'post',
        headers: {
            "content-type": 'application/json'
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            sessionStorage.clear();
            localStorage.clear();

        },
        error: function(data) {
            sessionStorage.clear();
            localStorage.clear();
            window.location.replace("index.html");
        }
    }).done(function(dataJson) {
        window.location.replace("index.html");
    });
} //logout fn starts here

//function to load nav bar 
function loadnavbar() {
    var postData = JSON.stringify({
        "status": 1,
        "search": "",
        "start_date": "",
        "end_date": ""
    });

    var postData1 = JSON.stringify({
        "status": 3,
        "search": "",
        "start_date": "",
        "end_date": ""
    });

    $.ajax({
        url: loaddetailsebl_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            if (data.results.length != 0) {
                $('.enqhref').show();

                if (data.results.length < 4) {
                    for (var i = 0; i < data.results.length; i++) {
                        var created_on1 = data.results[i].created_on;
                        var datalogin1 = created_on1.split('T')
                        var content1 = datalogin1[0].split('-');
                        var mon = content1[1] - 1;
                        var date_join = content1[2] + '&nbsp' + month[mon] + '&nbsp' + content1[0];

                        var enddat = tConvert(datalogin1[1].slice(0, 8));

                        $('#enquiries_list').append(`  <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="images/download.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>${data.results[i].service.user.first_name}</h5> <span class="mail-desc">${date_join}/${enddat}</span> </div>
                                    </a>`);
                    }
                } else {
                    for (var i = 0; i < 4; i++) {
                        var created_on1 = data.results[i].created_on;
                        var datalogin1 = created_on1.split('T')
                        var content1 = datalogin1[0].split('-');
                        var mon = content1[1] - 1;
                        var date_join = content1[2] + '&nbsp' + month[mon] + '&nbsp' + content1[0];

                        var enddat = tConvert(datalogin1[1].slice(0, 8));

                        $('#enquiries_list').append(`  <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="images/download.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>${data.results[i].service.user.first_name}</h5> <span class="mail-desc">${date_join}/${enddat}</span> </div>
                                    </a>`);
                    }
                }
            } else {
                $('#enquiries_list').append(`<center><div class="drop-title">No Enquiries</div></center>`);
                $('.enqhref').hide();
            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });

    $.ajax({
        url: loaddetailsebl_api,
        type: 'post',
        data: postData1,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            if (data.results.length != 0) {
                $('.bookingclass').show();

                if (data.results.length < 4) {
                    for (var i = 0; i < data.results.length; i++) {
                        var created_on1 = data.results[i].created_on;
                        var datalogin1 = created_on1.split('T')
                        var content1 = datalogin1[0].split('-');
                        var mon = content1[1] - 1;
                        var date_join = content1[2] + '&nbsp' + month[mon] + '&nbsp' + content1[0];

                        var enddat = tConvert(datalogin1[1].slice(0, 8));
                        $('#booking_load').append(` <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="images/bookings.png" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>${data.results[i].service.user.first_name}</h5> <span class="mail-desc">${data.results[i].service.name}</span> <span class="time">${date_join}/${enddat}</span> </div>
                                    </a>`);
                    }
                } else {
                    for (var i = 0; i < 4; i++) {
                        var created_on1 = data.results[i].created_on;
                        var datalogin1 = created_on1.split('T')
                        var content1 = datalogin1[0].split('-');
                        var mon = content1[1] - 1;
                        var date_join = content1[2] + '&nbsp' + month[mon] + '&nbsp' + content1[0];

                        var enddat = tConvert(datalogin1[1].slice(0, 8));
                        $('#booking_load').append(`   <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="images/bookings.png" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>${data.results[i].service.user.first_name}</h5> <span class="mail-desc">${data.results[i].service.name}</span> <span class="time">${date_join}/${enddat}</span> </div>
                                    </a>`);
                    }
                }
            } else {
                $('#booking_load').append(`<center><div class="drop-title">No Booking</div></center>`);
                $('.bookingclass').hide();

            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
}


///TIME CONVERTION FUNCTION
function tConvert(time) {
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
        time = time.slice(1);
        time[3] = +time[0] < 12 ? 'AM' : 'PM';
        time[0] = +time[0] % 12 || 12;
    }
    return time.join('');
}


var details = JSON.parse(localStorage.userdetails);
var name = (details.first_name == '') ? details.username : details.first_name;
$('.username').text(name);
$('.emailid_index').text(details.email);

$('.sendldr').hide();
loadnavbar();

function pagesallowed(){
    var x = 0;
    var allowed = JSON.parse(localStorage.userdetails).permissions;
    if(allowed.length){
        for(var i = 0; i < allowed.length; i++){
            if(location.pathname.split("/")[location.pathname.split("/").length - 1] == pageslist[allowed[i].permission.id]){
                x++;
            }
        }
        if(!x){
            window.location.href = "dashboard.html";
        }
    }
}

function commmonErrmsg(data){
    for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
}