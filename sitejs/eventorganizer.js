var page_no = 1;
var organizer_id;

var filter = {
    limit: 24
};

var datetime = function datetime(x) {
    return new Date(x).toLocaleString('en-IN', { year: 'numeric', day: '2-digit', month: 'short', hour: 'numeric', minute: 'numeric', hour12: true });
};

const createQuery = () => {
    filter.offset = page_no > 1 ? filter.limit * ( page_no - 1 ) : '';
    let query = [];
    for(var keys in filter){
        query.push(keys + "=" + filter[keys]);
    }
    return '?' + query.join('&');
}

$(() => {
    listData( eventorganizer_api + createQuery() );
    listOrganizerData( organizer_api );
    dateFilter();
    $('#organizermodal').on('hidden.bs.modal', function (e) {
        reloadData();
    })
})

const listData = Url => {
    $(".dataldr").show(); 
    $.ajax({
        url: Url,
        type: "GET",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        }
    }).done(data => {
        $(".dataldr").hide();     
        document.getElementById("listTable").innerHTML = tableComponent(data.results);
        switchery();        
        pagination(data.next, data.previous, data.count);
    }).fail(data => {
        console.log(data);
        $(".paginator").empty();       
        $(".dataldr").hide();             
    })
}


const tableComponent = data => ( data.length ?
`<table class="table table-hover manage-u-table headclassuser">
    <thead>
        <tr>
            <th width="70" class="text-center">S.No</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Time to call</th>
            <th>Created On</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        ${tableBody(data)}
    </tbody>
</table>` : 
`<center>
    <img src="images/logo.png" alt="home" class="light-logo" style="width:20% !important">
    <br><p class="classnodata">No Data Found</p>
</center>`);


const tableBody = data => {
    let row = ``;
    data.map((tr, index) => {
        row += 
        `<tr>
            <td class="text-center">${( (page_no - 1) * filter.limit ) + index + 1}</td>
            <td>${tr.name}</td>
            <td>${tr.mobile_number}</td>
            <td>${tr.email}</td>
            <td>${tr.time_to_call}</td>
            <td>${datetime(tr.created_on)}</td>
            <td>
                <input type="checkbox" class="js-switch commonclass" data-color="#f96262" data-size="small" ${tr.is_closed ? "checked" : ""} onchange="closeOrganizer(${tr.id}, ${tr.is_closed});"/>
            </td>
        </tr>`
    })
    return row;
}

// Switchery
const switchery = () => {
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });
}

//pagination starts here
const pagination = (next, prev, count) => {
    $(".paginator").empty();
    var pages = Math.ceil(count / filter.limit);
    if (pages > 1) {
        prev ? $(".paginator").append(`<li><a onclick="prevpage('${prev}')"><i class="fa fa-angle-left"></i></a></li>`) : '';
        for (var i = (page_no < 3 ? 1 : page_no - 2); i <= pages && i <= (page_no < 3 ? page_no + (5 - page_no) : page_no + 2); i++) {
            $(".paginator").append(`<li class="pgs pg${i}"><a onclick="navigate(${i})">${i}</a></li>`);
        }
        next ? $(".paginator").append(`<li><a onclick="nextpage('${next}')"><i class="fa fa-angle-right"></i></a></li>`) : '';
    }
    $(".pgs").removeClass("active");
    $(".pg" + page_no).addClass("active");
}

const navigate = page => {
    page_no = Number(page);
    listData( eventorganizer_api + createQuery() );
}

const nextpage = next => {
    page_no++;
    listData(next);
}

const prevpage = prev => {
    page_no--;
    listData(prev);
}

//filters
const reloadData = () => {
    page_no = 1;    
    listData( eventorganizer_api + createQuery() );    
}

const nameFilter = () => {
    filter.q = $(".searchbyname").val();
    reloadData();
}

const dateFilter = () => {
    $('.input-daterange-datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        filter.created_min = picker.startDate.format('YYYY-MM-DD');
        filter.created_max = picker.endDate.format('YYYY-MM-DD');
        reloadData();
    });
}

const showallfunc = () => {
    filter.q = '';
    filter.created_min = '';
    filter.created_max = '';
    $(".searchbyname, .input-daterange-datepicker").val('');
    reloadData();
}

function exportascsv(){
    window.location.href = eventorganizer_api + "export/" + createQuery();
}

const closeOrganizer = (id, is_closed) => {
    if(!is_closed){
        organizer_id = id;
        $("#organizermodal").modal('show');
    } else {
        reopen(id);
    }
}

const listOrganizerData = Url => {
    $(".dataldr").show(); 
    $.ajax({
        url: Url,
        type: "GET",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        }
    }).done(data => {     
        document.getElementById("organizerlistTable").innerHTML = organizertableComponent(data);
    }).fail(data => {
        console.log(data);                   
    })
}

const organizertableComponent = data => ( data.length ?
`<table class="table table-hover manage-u-table headclassuser">
    <thead>
        <tr>
            <th width="70" class="text-center">S.No</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Created On</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        ${organizertableBody(data)}
    </tbody>
</table>` : 
`<center>
    <img src="images/logo.png" alt="home" class="light-logo" style="width:20% !important">
    <br><p class="classnodata">No Data Found</p>
</center>`);


const organizertableBody = data => {
    let row = ``;
    data.map((tr, index) => {
        row += 
        `<tr class="active${tr.is_active}">
            <td class="text-center">${index + 1}</td>
            <td>${tr.name}</td>
            <td>${tr.mobile_number}</td>
            <td>${tr.email}</td>
            <td>${datetime(tr.created_on)}</td>
            <td style="padding: 8px;"><button class="btn btn-info btn-rounded closeorganizerbtn" style="padding: 5px 15px;" onclick="chooseOrganizer(${tr.id})">Close</button></td>
        </tr>`
    })
    return row;
}

const chooseOrganizer = organizer => {
    $(".closeorganizerbtn").attr("disabled", true);
    $.ajax({
        url: eventorganizer_api + organizer_id + "/",
        type: "PATCH",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        data: JSON.stringify({
            organizer,
            is_closed: true
        })
    }).done(data => {
        $(".closeorganizerbtn").attr("disabled", false);
        $(".orgClose").click();        
        $("#snackbarsuccs").text("Closed");
        showsuccesstoast();
    }).fail(data => {
        $(".closeorganizerbtn").attr("disabled", false);
        console.log(data); 
        commmonErrmsg(data);       
    })
}

const reopen = pk => {
    $.ajax({
        url: organizerreopen_api + pk + "/",
        type: "post",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        }
    }).done(data => {
        reloadData();
        $("#snackbarsuccs").text("Reopened");
        showsuccesstoast();
    }).fail(data => {
        console.log(data);
        commmonErrmsg(data);          
    })
}