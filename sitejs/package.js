$(function() {
    listpackages();
    $(".editldr,.addldr").hide();
    $(".editdip,.editip").keyup(function(e){
        $(this).val()?$(this).removeClass("iserror"):$(this).addClass("iserror");
        e.keyCode == 13 ? $(".editbtn").click():'';
    });
    $(".selectip,.descip").keyup(function(e){
        $(this).val().trim()?$(this).removeClass("iserror"):$(this).addClass("iserror");
        e.keyCode == 13 ? $(".addbtn").click():'';
    });
});

function addfeatures() {
    $(".desclist").append(`
        <div class="col-md-10 extradesc">
            <textarea class="form-control form-control-line featip descip">
            </textarea><br>
        </div>
    `);
}


function addpackage() {
    for (var i = 0; i < $(".selectip").length; i++) {
        if ($(".selectip").eq(i).val() == "") {
            $(".selectip").eq(i).addClass("iserror");
            $("#snackbarerror").text($(".selectlabel").eq(0).text() + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }
    for (var i = 0; i < $(".descip").length; i++) {
        if ($(".descip").eq(i).val().trim() == "") {
            $(".descip").eq(i).addClass("iserror");
            $("#snackbarerror").text("Feature is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }
    var features = [];
    for (var i = 0; i < $(".descip").length; i++) {
        features.push({ description: $(".descip").eq(i).val() });
    }
    var postData = {
        name: $(".selectip").eq(0).val(),
        amount: $(".selectip").eq(1).val(),
        count: $(".selectip").eq(2).val(),
        features: features
    };
    $(".addbtn").attr("disabled", true);
    $(".addldr").show();
    $.ajax({
        url: addpackage_api,
        type: "post",
        data: JSON.stringify(postData),
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: function(data) {
            $(".addbtn").attr("disabled", false);
            $(".addldr").hide();
            $(".selectip").val('');
            $(".extradesc").remove();
            $(".descip").val('');
            listpackages();
            $("#snackbarsuccs").text("Package has been created successfully");
            showsuccesstoast();
        },
        error: function(data) {
            $(".addbtn").attr("disabled", false);
            $(".addldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function listpackages() {
    $.ajax({
        url: addpackage_api,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
        },
        success: function(data) {
            console.log(data);
            $(".listpackages").empty();
            for (var i = 0; i < data.length; i++) {
                $(".listpackages").append(`
                    <tr>
                        <td class="td${data[i].id}">${data[i].name}</td>
                        <td class="td${data[i].id}">Rs. ${data[i].amount}</td>
                        <td class="features_list${i}">
                            
                        </td>
                        <td class="td${data[i].id}">${data[i].count}</td>
                        <td><i class="fa fa-pencil" aria-hidden="true" data-toggle="modal" data-target="#editslotsmodal" onclick="getinfo(${data[i].id})"></i></td>
                        <td>
                            <button class="btn ${data[i].is_active==true?'btn-success':'btn-danger'} activebtn" onclick="deletepackage(${data[i].id})">${data[i].is_active==true?'Active':'Inactive'}</button>
                        </td>
                    </tr>
                `);
                for (var j = 0; j < data[i].features.length; j++) {
                    $(".features_list" + i).append(`
                        <div class="ft${data[i].id}" data-value="${data[i].features[j].id}">${data[i].features[j].description}</div>
                    `);
                }
            }
        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}
var getid;

function getinfo(id) {
    getid = id;
    for (var i = 0; i < 3; i++) {
        $(".editip").eq(i).val($(".td" + id).eq(i).text().replace("Rs. ", ""));
    }
    $(".editdec").empty();
    for (var i = 0; i < $(".ft" + id).length; i++) {
        var x = $(".ft" + id).eq(i).attr("data-value");
        var y = $(".ft" + id).eq(i).text();
        $(".editdec").append(`<textarea type="text" data-id="${x}" class="form-control editdip" placeholder="Feature" rows="3">${y}</textarea><br>`);
    }
}

function addedit() {
    $(".editdec").append(`<textarea type="text" class="form-control editdip" placeholder="Feature" rows="3"></textarea><br>`);
}

function updateservice() {
    for (var i = 0; i < $(".editip").length; i++) {
        if ($(".editip").eq(i).val() == "") {
            $(".editip").eq(i).addClass("iserror");
            $("#snackbarerror").text($(".editip").eq(i).attr("placeholder") + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }
    for (var i = 0; i < $(".editdip").length; i++) {
        if ($(".editdip").eq(i).val() == "") {
            $(".editdip").eq(i).addClass("iserror");
            $("#snackbarerror").text($(".editdip").eq(i).attr("placeholder") + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }
    var edit = [];
    for (var i = 0; i < $(".editdip").length; i++) {
        edit.push({
            id: $(".editdip").eq(i).attr("data-id"),
            description: $(".editdip").eq(i).val()
        });
    }
    var postData = JSON.stringify({
        name: $(".editip").eq(0).val(),
        amount: $(".editip").eq(1).val(),
        count: $(".editip").eq(2).val(),
        features: edit
    });
    $(".editbtn").attr("disabled", true);
    $(".editldr").show();
    $.ajax({
        url: editpackage_api+getid+'/',
        type: "put",
        data: postData,
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: function(data) {
            $(".close").click();
            $(".editbtn").attr("disabled", false);
            $(".editldr").hide();
            listpackages();
            $("#snackbarsuccs").text("Package has been edited successfully");
            showsuccesstoast();
        },
        error: function(data) {
            $(".close").click();
            $(".editbtn").attr("disabled", false);
            $(".editldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function deletepackage(id){
    $.ajax({
        url: deletepackage_api+id+'/',
        type:"put",
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: function (data){
            listpackages();
            $("#snackbarsuccs").text(`Package has been ${data.is_active ? 'Activated': 'Deactivated'} successfully`);
            showsuccesstoast(); 
        },
        error: function(data){
           for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast(); 
        }
    });
}