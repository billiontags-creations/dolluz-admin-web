// var domain = "http://192.168.1.15:8000/";
var domain = "http://api.eventsunited.in/";


//AUTHENTICATION FUNCTION STARTS HERE
var login_api = domain + 'auth/login/';
var changepassword_api = domain + 'auth/change-password/';
var forgotpwd_api = domain + 'auth/forgot-password/';
var resetpassword_api = domain + 'auth/reset-password/';
var logout_api = domain + 'auth/logout/';

//DASHBOARD API
var dashboard_api = domain + 'super-admin/dashboard/';

//FREELANCER API 
var listfreelancer_api = domain + 'category/list/create/sub-category/?category=10';
var addfreelancers_api = domain + 'category/list/create/sub-category/';
var updatefreelancers_api = domain + 'category/update/sub-category/';

//MANAGE USER API
var listmanageruser_api = domain + 'super-admin/list/users/';
var useractiverdiactivate_api = domain + 'super-admin/activate-user/';
var resetpassworduser_api = domain + 'super-admin/reset-password/';

//LIST VENDORS API
var listvendor_api = domain + 'super-admin/list/vendors/';
var listservices_api = domain + 'super-admin/list/vendor-services/';

//LIST SUB VENDORS 
var listsubadmin_api = domain + 'super-admin/list/sub-admins/';
var createsubadmin_api = domain + 'super-admin/create/sub-admin/';
var setpassword_api = domain + 'super-admin/set-password/';
var listuserpermission_api = domain + 'super-admin/list/user-permissions/';
var updatepermission_api = domain + 'super-admin/update/permissions/';

//ADD SERVICE API
var listcategory_api = domain + 'category/list/create/sub-category/';
var listservicesmanage_api = domain + 'category/list/create/type/?sub_category=';
var addservice_api = domain + 'category/list/create/type/';
var updateservice_api = domain + 'category/update/type/';
var addservicemultiple_api =  domain + 'category/create/type/';

//ENQUIRIES,BOOKINGS,LEADS API
var loaddetailsebl_api = domain + 'bookings/list/service-bookings/';
var loadstatistics_api = domain + 'bookings/list/statistics/?status=';
//ACCOUNTS PAGE API
var listvendoracc_api = domain + 'accounts/list/vendors/';
var listdetails_api = domain + 'accounts/list/clearance-history/?user=';
var getdeatilsmodal_api = domain  + 'accounts/list/last-clearance/';
var postdataclearance_api = domain + 'accounts/create/clearance-history/';


//packages page
var addpackage_api = domain + 'packages/list/create/';
var editpackage_api = domain + 'packages/update/';
var deletepackage_api = domain + 'packages/delete/';

//manage services page
var listing_services_api = domain + "services/manage/"; 
var deactivateservice_api = domain + 'services/de-activate/';

var vendorcsv_api = domain + 'super-admin/download/vendors/';
var userscsv_api = domain + 'super-admin/download/users/';
var contactus_api = domain + 'contact_us/';
var eventspecialist_api = domain + 'event_specialist/';
var eventorganizer_api = domain + 'event_organizer/request/';
var organizer_api = domain + 'event_organizer/';
var organizerreopen_api = domain + "event_organizer/request/re-open/";

var pageslist = ["dashboard.html", "manage-users.html", "manage-vendors.html", "manage-subadmins.html", "manage-freelancers.html", "masters-management.html", "bookings.html", "leads.html", "accounts.html", "packages.html"];