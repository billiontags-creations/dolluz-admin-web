var addfreelancer = [],
    editfreelancer = [],
    editid;
$('.saveproductldr,.editproductldr').hide();
$(function() { //initial function starts here
    pagesallowed();    
    loadfreelancer();
    $('#freelanceid').change(function() {
        addfreelancer = $('#freelanceid')[0].files[0];
        $('.nameclass1').text($('#freelanceid')[0].files[0].name);
    })
    $('#editfreelancerimg').change(function() {
        editfreelancer = $('#editfreelancerimg')[0].files[0];
         $('.nameclass2').text($('#editfreelancerimg')[0].files[0].name);
    })
}); //initial function ends here

function loadfreelancer() {
    $.ajax({
        url: listfreelancer_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('.listfreelancers').empty();
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {
                    $('.listfreelancers').append(` <div class="col-sm-3">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <p class="addserviceheading dataname${data[i].id}" value="${data[i].transaction_percent}">${data[i].name}</p>
                                </div>
                                <div class="col-sm-2">
                                    <a onclick="editcategory(${data[i].id})" class="pointer savebtn">Edit</a>
                                </div>
                            </div>
                            <hr class="mt-0 mb-10">
                            <a class="freelancerimage" style="background-image:url('${data[i].image}')"></a>
                        </div>
                    </div>`)
                }
            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
} //load ends here

//add freelancer function starts here
function addfreelancerfunc() {
    var postData = new FormData();
    if ($('#freelancername').val() == '') {
        $('#freelancername').addClass("iserror");
        $("#snackbarerror").text("Freelance Name is required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("name", $('#freelancername').val());
    }
    if ($('#transperc').val() == '') {
        $('#transperc').addClass("iserror");
        $("#snackbarerror").text("Transaction Percentage is required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("transaction_percent", $('#transperc').val());

    }

    if (addfreelancer == '') {
        $("#snackbarerror").text("Image is required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("image", addfreelancer);
        postData.append("category", 8);
    }


    console.log(postData);
    $('.saveproductldr').show();
    $(".saveproductbtn").attr("disabled", true);

    $.ajax({
        url: addfreelancers_api,
        type: 'POST',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {

            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $("#snackbarsuccs").text("Product has been added successfully");
            showsuccesstoast();
            $('.saveproductldr').hide();
            $(".saveproductbtn").attr("disabled", false);
            $('.addfreelancerclose').click();
            $('#freelancername,#transperc').val('');
            addfreelancer = [];
            loadfreelancer();
        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
            $('.saveproductldr').hide();
            $(".saveproductbtn").attr("disabled", false);
        }
    });
} //add freelancer ends here

//edit category function
function editcategory(id) {
    editid = id;
    $('#freelancernameedit').val($('.dataname' + id).text());
    $('#transprecedit').val($('.dataname' + id).attr("value"));
    $('#editcategorymodal').modal();
}

//add freelancer function starts here
function editfreelancerfunc() {
    var postData = new FormData();
    if ($('#freelancernameedit').val() == '' && $('#transprecedit').val() == '') {
        $("#snackbarerror").text("Enter some content to edit");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("name", $('#freelancernameedit').val());
        postData.append("transaction_percent", $('#transprecedit').val());
        postData.append("category", 8);
    }
    if (editfreelancer != '') {
        postData.append("image", editfreelancer);
    }

    console.log(postData);
    $('.editproductldr').show();
    $(".editproductbtn").attr("disabled", true);

    $.ajax({
        url: updatefreelancers_api + editid + '/',
        type: 'put',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {

            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $("#snackbarsuccs").text("Product has been Updated successfully");
            showsuccesstoast();
            $('.editproductldr').hide();
            $(".editproductbtn").attr("disabled", false);
            $('.editfreelancerclose').click();
            editfreelancer = [];
            loadfreelancer();
        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
            $('.editproductldr').hide();
            $(".editproductbtn").attr("disabled", false);
        }
    });
} //add freelancer ends here