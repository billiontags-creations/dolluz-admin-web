color_array = ["bg-info", "bg-success", "bg-warning", "bg-danger", "bg-inverse"];

$(function() {
    dashboardfunc();
});

function dashboardfunc() {
    $.ajax({
        url: dashboard_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('.booking0').text(data.booking.today);
            $('.booking1').text(data.enquiry.today);
            $('.booking2').text(data.leads.today);
            $('.booking3').text(data.users.today);

            $('.bookings0').css("width", (data.booking.today / data.booking.total) * 100 + '%');
            $('.bookings1').css("width", (data.enquiry.today / data.booking.total) * 100 + '%');
            $('.bookings2').css("width", (data.leads.today / data.booking.total) * 100 + '%');
            $('.bookings3').css("width", (data.users.today / data.booking.total) * 100 + '%');

            $('.bookingweek0').text(data.booking.week);
            $('.bookingweek1').text(data.enquiry.week);
            $('.bookingweek2').text(data.leads.week);
            $('.bookingweek3').text(data.users.week);

            $('.bookingweeks0').css("width", (data.booking.week / data.booking.total) * 100 + '%');
            $('.bookingweeks1').css("width", (data.enquiry.week / data.booking.total) * 100 + '%');
            $('.bookingweeks2').css("width", (data.leads.week / data.booking.total) * 100 + '%');
            $('.bookingweeks3').css("width", (data.users.week / data.booking.total) * 100 + '%');

            $('.bookingmonth0').text(data.booking.month);
            $('.bookingmonth1').text(data.enquiry.month);
            $('.bookingmonth2').text(data.leads.month);
            $('.bookingmonth3').text(data.users.month);

            $('.bookingmonths0').css("width", (data.booking.month / data.booking.total) * 100 + '%');
            $('.bookingmonths1').css("width", (data.enquiry.month / data.booking.total) * 100 + '%');
            $('.bookingmonths2').css("width", (data.leads.month / data.booking.total) * 100 + '%');
            $('.bookingmonths3').css("width", (data.users.month / data.booking.total) * 100 + '%');

            $('.bookingyear0').text(data.booking.year);
            $('.bookingyear1').text(data.enquiry.year);
            $('.bookingyear2').text(data.leads.year);
            $('.bookingyear3').text(data.users.year);

            $('.bookingyears0').css("width", (data.booking.year / data.booking.total) * 100 + '%');
            $('.bookingyears1').css("width", (data.enquiry.year / data.booking.total) * 100 + '%');
            $('.bookingyears2').css("width", (data.leads.year / data.booking.total) * 100 + '%');
            $('.bookingyears3').css("width", (data.users.year / data.booking.total) * 100 + '%');


            // function initialfunc() {

            var chartvalue = [{
                "year": "January",
                "income": 0
            }, {
                "year": "Febraury",
                "income": 0
            }, {
                "year": "March",
                "income": 0
            }, {
                "year": "April",
                "income": 0
            }, {
                "year": "May",
                "income": 0
            }, {
                "year": "June",
                "income": 0
            }, {
                "year": "July",
                "income": 0
            }, {
                "year": "August",
                "income": 0
            }, {
                "year": "September",
                "income": 0
            }, {
                "year": "October",
                "income": 0
            }, {
                "year": "November",
                "income": 0
            }, {
                "year": "December",
                "income": 0
            }];

            repeatArray(color_array, data.sub_categories.length);
            for (var i = 0; i < data.sub_categories.length; i++) {
                $('#services_load').append(` <li><div class="categoryicon${i}"><i class="ti-user text-white"></i></div>  ${data.sub_categories[i].name}  <span class="text-muted">${data.sub_categories[i].count}</span></li>`);
                $('.categoryicon' + i).addClass(color_array[i]);
            }

            for (var j = 0; j < data.bookings_this_year.length; j++) {
                chartvalue[data.bookings_this_year[j].month - 1].income = data.bookings_this_year[j].count;
            }

            var chart = AmCharts.makeChart("chartdiv", {
                "theme": "light",
                "type": "serial",
                "dataProvider": chartvalue,
                "valueAxes": [{
                    "title": "Bookings"
                }],
                "graphs": [{
                    "balloonText": "Income in [[category]]:[[value]]",
                    "fillAlphas": 1,
                    "lineAlpha": 0.2,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                }],
                "depth3D": 1,
                "angle": 0,
                "rotate": true,
                "categoryField": "year",
                "categoryAxis": {
                    "gridPosition": "start",
                    "fillAlpha": 1,
                    "position": "left"
                },
                "export": {
                    "enabled": true
                }
            });
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}


function repeatArray(arr, count) {
    var ln = arr.length;
    for (i = 0; i < count; i++) {
        color_array.push(arr[i % ln]);
    }
}