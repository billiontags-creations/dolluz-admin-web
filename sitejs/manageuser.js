var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    toggleid, changeid;
$('.enableldr,.showalldr,.updateldr').hide();
var send_start, send_end, nxt;
$(function() { //initial function starts here
    pagesallowed();
    loadmanageuser(1);
    $("#adminpassword").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".enablebtn").click();
        }
    });
    $(".editpass").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".updatebtn").click();
        }
    });
    $('.input-daterange-datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        $('#urlloader').empty();
        $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
        send_start = picker.startDate.format('YYYY-MM-DD');
        send_end = picker.endDate.format('YYYY-MM-DD');
        loadmanageuser(2);
    });
}); //initial function ends here

function loadmanageuser(type) { //load manager function starts here
    if (type == 1) {
        var postData = JSON.stringify({
            "search": "",
            "start_date": "",
            "end_date": ""
        });
        var url = listmanageruser_api;
    } else if (type == 2) {
        var post = {};
        $('.searchbyname').val() ? post.search = $('.searchbyname').val() : '';
        send_start ? post.start_date = send_start : '' ; 
        send_end ? post.end_date = send_end : ''; 
        var postData = JSON.stringify(post);
        var url = listmanageruser_api;

    } else {
        var url = type;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('input[name="daterangepicker_start"]').val("");
            $('input[name="daterangepicker_end"]').val("");

            $('.showalldr,updateldr').hide();

            $('.userbodayclass,.imgappendclass').empty();
            // $('.imgappendclass').empty();

            if (data.results.length != 0) {
                $('.headclassuser').show();

                for (var i = 0; i < data.results.length; i++) {
                    //joined date

                    var datalogin1 = data.results[i].date_joined.split('T')
                    var content1 = datalogin1[0].split('-');
                    var date_join = content1[2] + ' ' + month[content1[1] - 1] + ' ' + content1[0];

                    var enddat = tConvert(datalogin1[1].slice(0, 8));

                    //last login date
                    if (data.results[i].last_login == null) {
                        var date_log = '-';
                        var startdat = '';
                    } else {

                        var datalogin = data.results[i].last_login.split('T')
                        var content = datalogin[0].split('-');
                        var date_log = content[2] + ' ' + month[content[1] - 1] + ' ' + content[0];

                        var startdat = tConvert(datalogin[1].slice(0, 8));
                    }

                    //validation for address
                    if (data.results[i].address == null) {
                        var area_name = "Not Mentioned";
                        // var city_name = "-";

                    } else {
                        var area_name = data.results[i].address.area.name + "," + data.results[i].address.city.name;
                        // var city_name = data.results[i].address.city.name;
                    }
                    $('.userbodayclass').append(` <tr class="colorappend${data.results[i].id}">
                                    <td class="text-center">${i+1}</td>
                                    <td>${data.results[i].first_name}
                                        <br/><span class="text-muted">${area_name}</span></td>
                                    }
                                    <td>${data.results[i].email}
                                        <br/><span class="text-muted">${data.results[i].username}</span></td>
                                    <td>
                                        <a onclick="getchangeid(${data.results[i].id});" class="viewanchor cptr" data-toggle="modal" data-target="#editpwdmodal">Edit Password</a>
                                    </td>
                                    <td>800</td>
                                    <td>${date_log}
                                        <br/><span class="text-muted">${startdat}</span></td>
                                    }
                                    <td>${date_join}
                                        <br/><span class="text-muted">${enddat}</span></td>
                                    }
                                    <td>
                                        <input type="checkbox" class="js-switch commonclass switchclass${data.results[i].id}" value="${data.results[i].id}" onchange="myFunction(${data.results[i].id})" data-color="#f96262" data-size="small" />
                                    </td>
                                </tr>`);
                    (data.results[i].is_active) ? $('.switchclass' + data.results[i].id).attr("checked", "checked"): $('.colorappend' + data.results[i].id).addClass('colorclassactive');
                }
            } else {


                $('.headclassuser').hide();
                $('.imgappendclass').append(`<center><img src="images/logo.png" alt="home" class="light-logo" style="width": 20% !important><br><p class="classnodata">No Data Found</p></center>`);
            }

            if (data.next_url != null) {
                var next1 = (data.next_url).split('=');
                var val = data.count / data.page_size;
                if (val["colorField"] % 1 === 0) {
                    //if number is integer 
                    var val = val;
                } else {
                    var val = parseInt(val) + 1;
                }
                var obj = $('#pagination').twbsPagination({

                    totalPages: val,
                    visiblePages: 3,
                    onPageClick: function(event, page) {
                        console.info(page);
                        loadmanageuser(next1[0] + "=" + (page));
                    }
                });
                console.info(obj.data());
            }


            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
} //load manager function ends here




//TIME CONVERTION FUNCTION
function tConvert(time) {
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
        time = time.slice(1);
        time[3] = +time[0] < 12 ? 'AM' : 'PM';
        time[0] = +time[0] % 12 || 12;
    }
    return time.join('');
}

function myFunction(id) {
    // $(this).is(':checked');
    $('#conformationmodal').modal();
    console.log(id);
    toggleid = id;
}

function loaddata() {
    $('.switchclass' + toggleid).click();
}

function enabledisable() { //function starts here
    if ($('#adminpassword').val() == '') {
        $('#adminpassword').addClass("iserror");
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $('.enableldr').show();
    $('.enablebtn').attr("disabled", true);

    var postData = JSON.stringify({
        "password": $('#adminpassword').val()
    });

    $.ajax({
        url: useractiverdiactivate_api + toggleid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('#urlloader').empty();
            $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
            $('.enableldr').hide();
            $('.enablebtn').attr("disabled", false);
            loadmanageuser(1);
            $("#snackbarsuccs").text("User status changed successfully");
            showsuccesstoast();
            $('.adminpassclose').click();
            $('#adminpassword').val('')
        },
        error: function(data) {
            $('.enableldr').hide();
            $('.enablebtn').attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //function ends here

//ONCHANGE FUNCTION FOR SEARCH STARTS HRE
$('.searchbyname').keyup(function() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    loadmanageuser(2);
});

//showall filter func
function showallfunc() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    $('.searchbyname, .input-daterange-datepicker').val('');
    $('.showalldr').show();
    loadmanageuser(1);
}

//get id function starts here
function getchangeid(getid) {
    changeid = getid
}


//on password change function starts here
function changepass() {
    if ($('#newpass').val() == '') {
        $('#newpass').addClass("iserror");
        $("#snackbarerror").text("New Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#renewpass').val() == '') {
        $('#renewpass').addClass("iserror");
        $("#snackbarerror").text("Re-enter New Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $('.updateldr').show();
    $('.updatebtn').attr("disabled", true);

    var postData = JSON.stringify({
        "password": $('#renewpass').val()
    });

    $.ajax({
        url: resetpassworduser_api + changeid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('.updateldr').hide();
            $('.updatebtn').attr("disabled", false);
            loadmanageuser(1);
            $("#snackbarsuccs").text("Password changed successfully");
            showsuccesstoast();
            $('.editpassclose').click();
            $('#newpass,#newpass').val('')
        },
        error: function(data) {
            $('.updateldr').hide();
            $('.updatebtn').attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });

}

function exportascsv(){
    window.location.href = userscsv_api + `?search=${$('.searchbyname').val() ? $('.searchbyname').val() : ''}&start_date=${send_start ? send_start : '' }&end_date=${send_end ? send_end : '' }`;
}