var userid, amtclear;
$(function() { //intial func starts here
    pagesallowed();        
    listvendor();
    $('#lastclearnceid,.tablehead').hide();
    $('.imageclass').append(`<center><div class="panel-heading">Select a Vendor</div></center>`)
}); //intial func starts here

function loaddetails(type) {
    if (type == 1) {
        var url = listdetails_api + userid;
    } else if (type == 2) {
        var startdate = $('input[name="daterangepicker_start"]').val().split('/');
        var send_start = startdate[2] + '-' + startdate[0] + '-' + startdate[1];
        var endate = $('input[name="daterangepicker_end"]').val().split('/');
        var send_end = startdate[2] + '-' + startdate[0] + '-' + startdate[1];

        var url = listdetails_api + userid + '&from_date=' + send_start + '&to_date=' + send_end;
    } else {

        var url = type;
    }

    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('.showalldr').hide();

            $('.tablebody').empty();
            if (data.results.length != 0) {
                $('.imageclass,.tablebody').empty();
                $('#lastclearnceid,.tablebody,.tablehead').show();


                for (var i = 0; i < data.results.length; i++) {
                    //joined date
                    var created_on1 = data.results[i].created_on;
                    var datalogin1 = created_on1.split('T')
                    var content1 = datalogin1[0].split('-');
                    var mon = content1[1] - 1;
                    var date_join = content1[2] + '&nbsp' + month[mon] + '&nbsp' + content1[0];


                    $('.tablebody').append(` <tr>
                                    <td class="text-center">${i+1}</td>
                                    <td>${date_join}</td>
                                    <td>${data.results[i].leads}</td>
                                    <td>Rs. <span>${data.results[i].amount}</span></td>
                                    <td>Rs. <span>${data.results[i].discount}</span></td>
                                    <td>
                                        ${data.results[i].description}
                                    </td>
                                </tr>`)
                }
            } else {
                $('.imageclass').empty();
                $('#lastclearnceid,.tablehead').hide();
                $('.imageclass').append(`<center><img src="images/logo.png" alt="home" class="light-logo" style="width": 20% !important><br><p class="classnodata">No Data Found</p></center>`);

            }
            loadheader();

            if (data.next_url != null) {
                var next1 = (data.next_url).split('&');
                var val = data.count / data.page_size;
                if (val["colorField"] % 1 === 0) {
                    //if number is integer 
                    var val = val;
                } else {
                    var val = parseInt(val) + 1;
                }

                var obj = $('#pagination').twbsPagination({
                    totalPages: val,
                    visiblePages: 3,
                    onPageClick: function(event, page) {
                        console.info(page);
                        loaddetails(next1[0] + "&page=" + (page));
                    }
                });
                console.info(obj.data());
            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function loadheader() {
    $.ajax({
        url: getdeatilsmodal_api + userid + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            if (data.last_clearance_date != null) {
                var datalogin1 = data.last_clearance_date.split('T')
                var content1 = datalogin1[0].split('-');
                var date_join = content1[2] + '-' + month[content1[1] - 1] + '-' + content1[0];
            } else {
                var date_join = "-";
            }
            var amt = (data.amount_from_prev_clearance == null) ? 'Rs.' + 0 : 'Rs.' + data.amount_from_prev_clearance;
            var earned = (data.total_earned == null) ? '0' : data.total_earned;



            $('.createddate').text(date_join);
            $('.leadscount').text(data.bookings_from_last_clearance);
            $('.amountcount').text(amt);


            // {
            //     "admin_cut": 600,
            //     "bookings_from_last_clearance": 0,
            //     "total_bookings": 3,
            //     "total_earned": 20000,
            //     "amount_from_prev_clearance": null,
            //     "last_clearance_date": "2017-10-20T16:42:03.495892",
            //     "user": {
            //         "first_name": "Gopi Akshay",
            //         "username": "8610011893"
            //     }
            // }
            amtclear = (data.amount_from_prev_clearance == null) ? 0 : (data.amount_from_prev_clearance - data.admin_cut);

            //card load data
            $('.nameclassmodal').text(data.user.first_name);
            $('.usernameclassmodal').text(data.user.username);
            $('.earnedclassmodal').text(earned);
            $('.clearclassmodal').text(date_join);
            $('.prevleadclassmodal').text(data.total_bookings);
            $('.prevamtclassmodal').text(amt);
            $('.discountclassmodal').text(data.admin_cut);

        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

//list vendor function starts here
function listvendor() {
    $.ajax({
        url: listvendoracc_api,
        type: 'get',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('#selectload').empty();
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#selectload').append((i == 0) ? `<option value="0">Select Vendor</option><option value="${data[i].id}">${data[i].first_name}</option>` : `<option value="${data[i].id}">${data[i].first_name}</option>`);
                }
            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}


//load details on change
$('#selectload').change(function() {
    if ($('#selectload').val() != 0) {
        userid = $('#selectload').val();
        $('.imageclass,#urlloader').empty();
        $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
        $('#lastclearnceid,table').show();
        loaddetails(1);
    } else {
        $('#lastclearnceid,table').hide();
        $('.imageclass').append(`<center><div class="panel-heading">Select a Vendor</div></center>`);
    }
})

//function to check
function checkfunc() {
    if ($('#selectload').val() != 0) {
        $('#listclearancemodal').modal();
    } else {
        $("#snackbarerror").text("Select a vendor to view the deatils");
        showerrtoast();
    }
}

//search on change for date picker starts here
$('.datepickerinput').change(function() {
    if ($('#selectload').val() != 0) {
        loaddetails(2);
    }
})

//search on change for date picker ends here

//post data starts here
function submitclearance() {

    var postData = JSON.stringify({
        "password": $('#passcheck').val(),
        "discount": $('#fineamt').val(),
        "description": $('#descriptionclassmodal').val(),
        "amount": amtclear,
        "user": userid
    });


    $.ajax({
        url: postdataclearance_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $("#snackbarsuccs").text("Clearance done successfully");
            showsuccesstoast();
            loaddetails(1);
            $('.modalclose').click();
            $('#passcheck,#fineamt,#descriptionclassmodal').val("");
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });

}
//post data ends here

function showallfunc() {
    if ($('#selectload').val() != 0) {
        $('.showalldr').show();
        loaddetails(1);
    } else {
        $("#snackbarerror").text("Select a vendor to view the deatils");
        showerrtoast();
    }
}