var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    toggleid, changeid, datatosend = [],
    updateper;
$('.enableldr,.showalldr,.updateldr,.addsubadminldr,.assignldr').hide();
$(function() { //initial function starts here
    pagesallowed();    
    loadmanagesubadmin(1);
    $("#adminpassword").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".enablebtn").click();
        }
    });
    $(".editpass").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".updatebtn").click();
        }
    });
}); //initial function ends here

function loadmanagesubadmin(type) { //load manager function starts here
    if (type == 1) {
        var postData = JSON.stringify({
            "search": "",
            "start_date": "",
            "end_date": ""
        });
        var url = listsubadmin_api;
    } else if (type == 2) {
        if ($('input[name="daterangepicker_start"]').val() != '' || $('input[name="daterangepicker_end"]').val() != '') {
            var startdate = $('input[name="daterangepicker_start"]').val().split('/');
            var send_start = startdate[2] + '-' + startdate[0] + '-' + startdate[1];
            var endate = $('input[name="daterangepicker_end"]').val().split('/');
            var send_end = startdate[2] + '-' + startdate[0] + '-' + startdate[1];
        }
        var postData = JSON.stringify({
            "search": $('.searchbyname').val(),
            "start_date": send_start,
            "end_date": send_end
        });
        var url = listsubadmin_api;

    } else {
        var url = type;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('input[name="daterangepicker_start"]').val("");
            $('input[name="daterangepicker_end"]').val("");

            $('.showalldr,updateldr').hide();
            $('.userbodayclass').empty();
            $('.imgappendclass').empty();

            if (data.results.length != 0) {
                $('.headclassuser').show();

                for (var i = 0; i < data.results.length; i++) {

                    //joined date
                    var datalogin1 = data.results[i].date_joined.split('T')
                    var content1 = datalogin1[0].split('-');
                    var date_join = content1[2] + ' ' + month[content1[1] - 1] + ' ' + content1[0];

                    var enddat = tConvert(datalogin1[1].slice(0, 8));
                    //last login date
                    if (data.results[i].last_login == null) {
                        var date_log = '-';
                        var startdat = '';
                    } else {
                        var datalogin = data.results[i].last_login.split('T')
                        var content = datalogin[0].split('-');
                        var date_log = content[2] + ' ' + month[content[1] - 1] + ' ' + content[0];

                        var startdat = tConvert(datalogin[1].slice(0, 8));
                    }


                    $('.userbodayclass').append(` <tr class="colorappend${data.results[i].id}">
                                    <td class="text-center">${i+1}</td>
                                    <td>${data.results[i].first_name}</td>
                                    <td>${data.results[i].username}</td>
                                    <td>
                                         <a onclick="getchangeid(${data.results[i].id});" class="viewanchor cptr" data-toggle="modal" data-target="#editpwdmodal">Edit Password</a>
                                    </td>
                                    <td><a onclick="listpermission(${data.results[i].id})" class="viewanchor cptr" data-toggle="modal" data-target="#allpermissionsmodal">Click Here</a></td>
                                    <td>${date_log}
                                        <br/><span class="text-muted">${startdat}</span></td>
                                    <td>${date_join}
                                        <br/><span class="text-muted">${enddat}</span></td>
                                    <td>
                                       <input type="checkbox" class="js-switch commonclass switchclass${data.results[i].id}" value="${data.results[i].id}" onchange="myFunction(${data.results[i].id})" data-color="#f96262" data-size="small" />
                                    </td>
                                </tr>`);


                    (data.results[i].is_active) ? $('.switchclass' + data.results[i].id).attr("checked", "checked"): $('.colorappend' + data.results[i].id).addClass('colorclassactive');
                }
            } else {


                $('.headclassuser').hide();
                $('.imgappendclass').append(`<center><img src="images/logo.png" alt="home" class="light-logo" style="width": 20% !important><br><p class="classnodata">No Data Found</p></center>`);
            }

            if (data.next_url != null) {
                var next1 = (data.next_url).split('=');
                var val = data.count / data.page_size;
                if (val["colorField"] % 1 === 0) {
                    //if number is integer 
                    var val = val;
                } else {
                    var val = parseInt(val) + 1;
                }
                var obj = $('#pagination').twbsPagination({
                    totalPages: val,
                    visiblePages: 3,
                    onPageClick: function(event, page) {
                        console.info(page);
                        loadmanagesubadmin(next1[0] + "=" + (page));
                    }
                });
                console.info(obj.data());
            }



            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
} //load manager function ends here


//list permission starts here
function listpermission(perid) {
    updateper = perid;
    $.ajax({
        type: 'GET',
        url: listuserpermission_api + perid + '/',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
    }).done(function(data) {
        $('input[name="checkbox_edit"]').prop("checked", false);
        for (var i = 0; i < data.length; i++) {
            if (data[i].is_active) {
                $('input[name="checkbox_edit"]').eq(data[i].id - 1).prop("checked", true);
            }
        }
    }).fail(function(data) {
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}


//update permission function starts here
function assignpermissionfunc() {
    $('.assignldr').show();
    datatosend = [];
    
    for (var i = 0; i < $('input[name="checkbox_edit"]').length; i++) {
        if ($('input[name="checkbox_edit"]').eq(i).prop("checked") == true) {
            datatosend.push($('input[name="checkbox_edit"]').eq(i).val());
        }
    }
    var postData = JSON.stringify({ "permissions": datatosend });
    $.ajax({
        url: updatepermission_api + updateper + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('.assignldr').hide();
            $('input[name="checkbox_edit"]').prop("checked", false);
            $('.assgnperclose').click();
            $("#snackbarsuccs").text("User permission updated");
            showsuccesstoast();

        },
        error: function(data) {
            $('.assignldr').hide();
            $('.assgnperclose').click();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

//TIME CONVERTION FUNCTION
function tConvert(time) {
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
        time = time.slice(1);
        time[3] = +time[0] < 12 ? 'AM' : 'PM';
        time[0] = +time[0] % 12 || 12;
    }
    return time.join('');
}

function myFunction(id) {
    // $(this).is(':checked');
    $('#conformationmodal').modal();
    console.log(id);
    toggleid = id;
}

function enabledisable() { //function starts here
    if ($('#adminpassword').val() == '') {
        $('#adminpassword').addClass("iserror");
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $('.enableldr').show();
    $('.enablebtn').attr("disabled", true);

    var postData = JSON.stringify({
        "password": $('#adminpassword').val()
    });

    $.ajax({
        url: useractiverdiactivate_api + toggleid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('#urlloader').empty();
            $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
            $('.enableldr').hide();
            $('.enablebtn').attr("disabled", false);
            loadmanagesubadmin(1);
            $("#snackbarsuccs").text("User status changed successfully");
            showsuccesstoast();
            $('.adminpassclose').click();
            $('#adminpassword').val('')
        },
        error: function(data) {
            $('.enableldr').hide();
            $('.enablebtn').attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //function ends here

//ONCHANGE FUNCTION FOR SEARCH STARTS HRE
$('.datepickerclass').change(function() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    loadmanagesubadmin(2);
});


$('.searchbyname').keyup(function() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    loadmanagesubadmin(2);
})

//showall filter func
function showallfunc() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    $('.searchbyname').val('');
    $('.showalldr').show();
    loadmanagesubadmin(1);
}


function loaddata() {
    $('.switchclass' + toggleid).click();
}

//get id function starts here
function getchangeid(getid) {
    changeid = getid
}


//on password change function starts here
function changepass() {
    if ($('#newpass').val() == '') {
        $('#newpass').addClass("iserror");
        $("#snackbarerror").text("New Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#renewpass').val() == '') {
        $('#renewpass').addClass("iserror");
        $("#snackbarerror").text("Re-enter New Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $('.updateldr').show();
    $('.updatebtn').attr("disabled", true);

    var postData = JSON.stringify({
        "password": $('#renewpass').val()
    });

    $.ajax({
        url: resetpassworduser_api + changeid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('.updateldr').hide();
            $('.updatebtn').attr("disabled", false);
            loadmanagesubadmin(1);
            $("#snackbarsuccs").text("Password changed successfully");
            showsuccesstoast();
            $('.editpassclose').click();
            $('#newpass,#renewpass').val('')
        },
        error: function(data) {
            $('.updateldr').hide();
            $('.updatebtn').attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });

}

//listing function starts here
function listing(listid) {
    $.ajax({
        type: 'GET',
        url: listservices_api + listid + '/',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
    }).done(function(data) {
        $('.listservice').empty();
        for (var i = 0; i < data.length; i++) {
            $('.listservice').append(` <li>
                                <div class="bg-info"><i class="fa fa-university text-white"></i></div> ${data[i].name} <span class="text-muted"> ${data[i].count} </span></li>
                            <li>`);
        }
    }).fail(function(data) {
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}

//addsubadmin function starts here
function addsubadmin() {
    datatosend = [];
    
    if ($('#nameid').val() == '') {
        $('#nameid').addClass("iserror");
        $("#snackbarerror").text("Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#phonenumberid').val() == '') {
        $('#phonenumberid').addClass("iserror");
        $("#snackbarerror").text("Phone Number is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#emailid').val() == '') {
        $('#emailid').addClass("iserror");
        $("#snackbarerror").text("Email Id is Required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        var x = $('#emailid').val();
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
            $('#emailid').addClass("iserror");
            $("#snackbarerror").text("Valid E-Mail Address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
    }
    if ($('#passwordid').val() == '') {
        $('#passwordid').addClass("iserror");
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('input[name="checkboxadd"]:checked').length == 0) {
        $("#snackbarerror").text('Permissions are required');
        showerrtoast();
        event.stopPropagation();
        return;
    } else {
        for (var i = 0; i < $('input[name="checkboxadd"]').length; i++) {
            if ($('input[name="checkboxadd"]').eq(i).prop("checked") == true) {
                datatosend.push({ 'permission': $('input[name="checkboxadd"]').eq(i).val() });
            }
        }
    }

    $('.addsubadminldr').show();
    $('.addsubadminbtn').attr("disabled", true);

    var postData = JSON.stringify({
        "first_name": $('#nameid').val(),
        "username": $('#phonenumberid').val(),
        "email": $('#emailid').val(),
        "password": $('#passwordid').val(),
        "userprofile": {
            "role": 2
        },
        "permissions": datatosend
    });

    $.ajax({
        url: createsubadmin_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $('#urlloader').empty();
            $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
            $('.addsubadminldr').hide();
            $('.addsubadminbtn').attr("disabled", false);
            loadmanagesubadmin(1);
            $("#snackbarsuccs").text("Sub admin has been added successfully");
            showsuccesstoast();
            $('.addsubmodalclose').click();
            $('#adminpassword').val('')
        },
        error: function(data) {
            $('.addsubadminldr').hide();
            $('.addsubadminbtn').attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}