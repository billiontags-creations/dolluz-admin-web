var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

$(function() { //initial function starts here
    pagesallowed();        
    $('.showalldr').hide();
    sidebar();
    loadleads(1);
}) //initial function ends here

function loadleads(type) {
    if (type == 1) {
        var postData = JSON.stringify({
            "status": 2,
            "search": "",
            "start_date": "",
            "end_date": ""
        });
        var url = loaddetailsebl_api;
    } else if (type == 2) {
        if ($('input[name="daterangepicker_start"]').val() != '' || $('input[name="daterangepicker_end"]').val() != '') {
            var startdate = $('input[name="daterangepicker_start"]').val().split('/');
            var send_start = startdate[2] + '-' + startdate[0] + '-' + startdate[1];
            var endate = $('input[name="daterangepicker_end"]').val().split('/');
            var send_end = startdate[2] + '-' + startdate[0] + '-' + startdate[1];
        }
        var postData = JSON.stringify({
            "status": 2,
            "search": $('#search_id').val(),
            "start_date": send_start,
            "end_date": send_end
        });
        var url = loaddetailsebl_api;
    } else {
        var postData = JSON.stringify({
            "status": 2,
            "search": "",
            "start_date": "",
            "end_date": ""
        });

        var url = type;
    }
    $.ajax({
        url: url,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            // $('input[name="daterangepicker_start"]').val("");
            // $('input[name="daterangepicker_end"]').val("");

            $('.showalldr').hide();

            $('.imgappendclass,.tablebody').empty();
            if (data.results.length != 0) {

                $('.tablehead').show();

                for (var i = 0; i < data.results.length; i++) {

                    var datalogin1 = data.results[i].created_on.split('T');
                    var content1 = datalogin1[0].split('-');
                    var date_join = content1[2] + ' ' + month[content1[1] - 1] + ' ' + content1[0];

                    var enddat = tConvert(datalogin1[1].slice(0, 8));

                    //validation for address
                    if (data.results[i].address == null) {
                        var area_name = "Not Mentioned";
                    } else {
                        var area_name = data.results[i].address.area.name + "," + data.results[i].address.city.name;
                    }
                    $('.tablebody').append(` <tr>
                                    <td class="text-center">${i+1}</td>
                                    <td>${data.results[i].user.first_name}
                                        <br/><span class="text-muted">${area_name}</span></td>
                                    }
                                    <td>${data.results[i].user.email}
                                        <br/><span class="text-muted">${data.results[i].user.username}</span></td>
                                    <td>${date_join}
                                        <br/><span class="text-muted">${enddat}</span>
                                    </td>
                                    <td>${data.results[i].service.name}</td>
                                   <td>${data.results[i].service.user.first_name} <br/><span class="text-muted">${data.results[i].service.user.username} </span></td>
                                    <td>
                                        <a class="findclass${data.results[i].id}"></a>
                                    </td>
                                </tr>`);
                    (data.results[i].is_viewed) ? $('.findclass' + data.results[i].id).addClass('viewed') && $('.findclass' + data.results[i].id).text('Viewed'): $('.findclass' + data.results[i].id).addClass('unviewed') && $('.findclass' + data.results[i].id).text('Yet to Viewed');
                }
            } else {
                $('.tablehead').hide();
                $('.imgappendclass').append(`<center><img src="images/logo.png" alt="home" class="light-logo" style="width": 20% !important><br><p class="classnodata">No Data Found</p></center>`);
            }

            if (data.next_url != null) {
                var next1 = (data.next_url).split('=');
                var val = data.count / data.page_size;
                if (val["colorField"] % 1 === 0) {
                    //if number is integer 
                    var val = val;
                } else {
                    var val = parseInt(val) + 1;
                }
                var obj = $('#pagination').twbsPagination({

                    totalPages: val,
                    visiblePages: 3,
                    onPageClick: function(event, page) {
                        console.info(page);
                        loadleads(next1[0] + "=" + (page));
                    }
                });
                console.info(obj.data());
            }
        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

//onchnage function starts here
//ONCHANGE FUNCTION FOR SEARCH STARTS HRE
$('.datepickerclass').change(function() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    loadleads(2);
});


$('#search_id').keyup(function() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    loadleads(2);
})

//show all function starts here
function showallfunc() {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="pagination m-b-20 floatright mr-20 liclassld pagination" id="pagination"></ul>`);
    $('input[name="daterangepicker_start"]').val("");
    $('input[name="daterangepicker_end"]').val("");

    $('#search_id').val("");
    $('.showalldr').show();
    loadleads(1);

}

///TIME CONVERTION FUNCTION
function tConvert(time) {
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
        time = time.slice(1);
        time[3] = +time[0] < 12 ? 'AM' : 'PM';
        time[0] = +time[0] % 12 || 12;
    }
    return time.join('');
}

//function to load side bar starts here
function sidebar() {
    $.ajax({
        url: loadstatistics_api + '2',
        type: 'get',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            var daily = (data.total == 0) ? 0 : (data.daily / data.total) * 100;
            var weekly = (data.total == 0) ? 0 : (data.weekly / data.total) * 100;
            var monthly = (data.total == 0) ? 0 : (data.monthly / data.total) * 100;


            $('.amt0').text(data.daily);
            $('.widthclass0').css("width", daily + "%");
            $('.textclass0').text(parseInt(daily) + "%");

            $('.amt1').text(data.weekly);
            $('.widthclass1').css("width", weekly + "%");
            $('.textclass1').text(parseInt(weekly) + "%");

            $('.amt2').text(data.monthly);
            $('.widthclass2').css("width", monthly + "%");
            $('.textclass2').text(parseInt(monthly) + "%");



        },
        failure: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}